# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/desmigor/s23-lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/desmigor/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

## Lab

Added CI/CD pipelines and they are passing.

## Homework

The app is autodeployed at [render.com](https://render.com/). It's available at [https://sqr-lab1-igor.onrender.com/hello](https://sqr-lab1-igor.onrender.com/hello)

